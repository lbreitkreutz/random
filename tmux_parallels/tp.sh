#!/usr/bin/env bash
################################################################################
#
################################################################################

#######################################
## CONSTANTS
declare -r VERSION="0.0.1";
declare -r __NAME__="Ansible-Parallel";
declare -r TMUX_SESSION_NAME="Ansible-Parallel";

declare -A COLORS;
COLORS["RED"]=$(tput setaf 1);
COLORS["GREEN"]=$(tput setaf 2);
COLORS["YELLOW"]=$(tput setaf 3);
COLORS["BLUE"]=$(tput setaf 4);
COLORS["PURPLE"]=$(tput setaf 5);
COLORS["CYAN"]=$(tput setaf 6);
COLORS["NORMAL"]=$(tput sgr0);
declare -r COLORS

declare -A BACKGROUNDS;
BACKGROUNDS["RED"]=$(tput setab 1);
BACKGROUNDS["GREEN"]=$(tput setab 2);
BACKGROUNDS["YELLOW"]=$(tput setab 3);
BACKGROUNDS["BLUE"]=$(tput setab 4);
BACKGROUNDS["PURPLE"]=$(tput setab 5);
BACKGROUNDS["CYAN"]=$(tput setab 6);
BACKGROUNDS["NORMAL"]=$(tput sgr0);
declare -r BACKGROUNDS;

declare -A MSG_STATUS;
MSG_STATUS["OKAY"]="[ OKAY ]";
MSG_STATUS["FAIL"]="[ FAIL ]";
MSG_STATUS["WARN"]="[ WARN ]";
declare -r MSG_STATUS;

declare -A DEFAULT;
DEFAULT["APT_SERVER"]="http://archive.ubuntu.com/ubuntu";
DEFAULT["COLUMNS"]=80;
DEFAULT["TAB"]="    ";
declare -r DEFAULT;

FULL_LINE="$(head -c "${DEFAULT["COLUMNS"]}" < /dev/zero | tr '\0' '#')";
declare -r FULL_LINE;

## END CONSTANTS
#######################################

display_status() {
    local command_exit_value="$1";
    local error_message="$2"
    local line="$3";

    if [[ ${command_exit_value} = "0" ]]; then
        printf '%s%*s%s\n' "${COLORS[GREEN]}" "$(( DEFAULT["COLUMNS"] - ${#line} ))" "${MSG_STATUS[OKAY]}" "${COLORS[NORMAL]}";
    else
        printf "%s%*s%s\\n" "${COLORS[RED]}" "$(( DEFAULT["COLUMNS"] - ${#line} ))" "${MSG_STATUS[FAIL]}" "${COLORS[NORMAL]}";
        printf "\\n%s\\n\\n" "${error_message}";
        exit 2;
    fi
}

ctrl_c() {
    local line;
    local fatal_message;
    line="Catch SIG-INT..."
    fatal_message="ERROR: Failed to catch SIG-INT.\\n\
        ... you should never see this error...";
    printf "\\b\\b%s" "${line}";
    display_status "0" "${fatal_message}" "${line}";

    printf "Exiting...\\n";
    exit 2;
}

#######################################
# Prints message and exits
# Globals:
#     NONE
# Arguments:
#     msg: Messsage to print before exiting
#     exit_code: exit code to return to OS upon program completion
#     status: should we print to stdout or stderror?
# Returns:
#     Terminates program and returns exit code to OS
#######################################
die() {
    # Message to print
    local msg="$1";
    local exit_code="$2";
    local status="$3";

    if [[ -z ${exit_code} ]]; then
        exit_code="2";
    fi

    # default status to ERROR
    if [[ "${status}" = "1" ]]; then
        exec 3>&1;
    else
        exec 3>&2;
    fi

    printf "%s\\n" "${msg}" >&3;

    # close custom file descriptor
    exec 3>&-;

    exit ${exit_code};
}

#######################################
# Prints help
# Globals:
#     __NAME__
# Arguments:
#     NONE
# Returns:
#     NONE
#######################################
show_help(){
    cat <<__EOF__
$(show_version)

Run the specified ansible playbook across all hosts in parallel
usage: ${__NAME__} [-i <PATH>] [-p <PATH>] [-s <PATH>]

Options:
    -h, --help
        Print detailed help screen
    -V, --version
        Print version information
    -i, --inventory-file
        Specify the ansible inventory file to use
    -s, --ansible-source
        Specify the ansible script to source
    -p, --playbook
        Ansible playbook to run
    -d, --debug
        Display debug info. Also enabled -v
    -v, --verbose
        Show verbose output
__EOF__
}

#######################################
# Prints program version
# Globals:
#     __NAME__
#     VERSION
# Arguments:
#     NONE
# Returns:
#     NONE
#######################################
show_version(){
    cat <<__EOF__    
${__NAME__}, version ${VERSION}
__EOF__
}

#######################################
# Parse a delimated string, and returns a globally parsable array
# Globals:
#     NONE
# Arguments:
#     $1: delimated string
# Returns:
#     newline delimated string
#######################################
parse_to_array() {
    local string="$1";
    local temp_string;
    local return_array;
    return_array=();

    local regex_comma="\\w,\\s*";
    local regex_colon="\\w\\:[^\\/\\/]\\s*";
    local regex_semicolon="\\w\\;\\s*";

    if [[ "${string}" =~ $regex_comma ]]; then
        # remove whitespace
        temp_string="$(printf "%s" "${string}" | tr -d ' ')"
        IFS="," read -ra return_array <<< "${temp_string}";
    elif [[ "${string}" =~ $regex_colon ]]; then
        # remove whitespace
        temp_string="$(printf "%s" "${string}" | tr -d ' ')"
        IFS=":" read -ra return_array <<< "${temp_string}";
    elif [[ "${string}" =~ $regex_semicolon ]]; then
        # remove whitespace
        temp_string="$(printf "%s" "${string}" | tr -d ' ')"
        IFS=";" read -ra return_array <<< "${temp_string}";
    else
        read -ra return_array <<< "${string}";
    fi

    # bash can't return anything in the normal sense
    # this includes variables, strings, integers, arrays, etc
    # so the best method is to echo a newline delimated string
    # this is "globally" parsable
    # meaning that we don't have to do anything special to get an array from it
    # I.E. array=("$(parse_to_array "${foo}")")
    printf "%s\\n" "${return_array[@]}";
}

toLower() {
    local string="$1";

    printf "%s" "${string,,}"
}

toUpper() {
    local string="$1";

    printf "%s" "${string^^}"
}

#######################################
# Parse /etc/hosts and dig for the hostname
# Globals:
#     NONE
# Arguments:
#     $1: IP or hostname
# Returns:
#     First configured hostname for the IP
#######################################
getHostname() {
    local ip="$1";
    # We query the hosts file.
    # first we filter out any commented entries
    # then we use extended regex to make sure that we get only the exact ip
    # then we get the second field, as the first is *always* the ip
    # finally, we use head to get the first in case there are multiple entries
    local hostname="$(grep -vie "^#" /etc/hosts | grep -E "${ip}\s+" | awk '{printf $2}' | head -n 1)";
    local hostname_dig="$(dig -x "${ip}" +noall +answer +short)";

    if [[ -n "${hostname}" ]]; then
        printf "%s" "${hostname}";
    elif [[ -n "${hostname_dig}" ]]; then
        printf "%s" "${hostname_dig}";
    else
        # if we can't find a hostname from /etc/hosts give back the IP
        printf "%s" "${ip}";
    fi
}

#######################################
# Program Entry Point
# Globals:
#     SERVICE_NAME
# Arguments:
#     $@: All arguments from std.in
# Returns:
#     int: exit code to OS as integer
#######################################
main() {
    local argv=("$@");
    # argc is the count of arguments
    local argc=${#argv[@]};

    # this is important to ensure globbing is active
    shopt -s extglob;


    # Handle compressed short options
    re="(^| )\\-[[:alnum:]]{2,}"; # regex to detect shortoptions
    # we evaluate this as a long string, thus ${argv[*]}, instead of ${argv[@]}
    if [[ "${argv[*]}" =~ $re ]]; then
        local compiled_args=();
        for ((i=0; i<argc; i++)); do
            if [[ "${argv[$i]}" =~ $re ]]; then
                local compressed_args="${argv[$i]#*-}";
                for ((r=0; r<${#compressed_args}; r++)); do
                    compiled_args+=("-${compressed_args:$r:1}");
                done
                shift;
                compiled_args+=("$@");
                ## recurse
                main "${compiled_args[@]}";
                ## we "pass" the exit code back up the recursions to the OS
                exit $?;
            fi
            compiled_args+=("${argv[$i]}");
            shift;
        done
        exit;
    fi
    ############################################################################
    ## Define argument variables

    # inventory file
    local arg_inventory_file;
    # ansible script to source
    local arg_ansible_source;
    # playbook to run
    local arg_ansible_playbook;

    ## END Define argument variables
    ############################################################################

        while :; do
        case $1 in
            "-h"|"-\\?"|"--help")
                show_help;    # Display a usage synopsis.
                exit 0;
                ;;
            "-V"|"--version")
                show_version;
                exit 0;
                ;;
            "-i"|"--inventory-file")
                arg_inventory_file="$2";
                shift;
                ;;
            --inventory-file=?*)
                arg_inventory_file="${1#*=}";
                ;;
            "-s"|"--ansible-source")
                arg_ansible_source="$2";
                shift;
                ;;
            --ansible-source=?*)
                arg_ansible_source="${1#*=}";
                ;;
            "-p"|"--playbook")
                arg_ansible_playbook="$2";
                shift;
                ;;
            --playbook=?*)
                arg_ansible_playbook="${1#*=}";
                ;;
            "-d"|"--debug")
                # if there is a value as the next arg,
                # instead of another argument
                if [[ "$2" =~ ^[^-] ]]; then
                    arg_debug="$2";
                else
                    ((arg_debug++));
                fi
                ;;
            --debug=?*)
                arg_debug="${1#*=}";
                ;;
            "-v"|"--verbose")
                ((arg_verbose++));
                ;;
            --)              # End of all options.
                shift
                break
                ;;
            -?*)
                printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
                ;;
            *)        # Default case: No more options, so break out of the loop.
                break
        esac
        
        shift
    done

    ############################################################################
    ## Set defaults
    ## Handle arguments

    # handle CTRL+C
    trap 'ctrl_c' INT;

    # file descriptor 3 is for debug
    if [[ -n ${arg_debug} ]]; then
        exec 3>&1;
        # enabling debug also enables verbose
        ((arg_verbose++));
    else
        exec 3>/dev/null;
    fi
    
    if (( arg_debug > 1 )); then
        set -x;
    fi

    # file descriptor 4 is for verbose
    if [[ -n ${arg_verbose} ]]; then
        exec 4>&1;
    else
        exec 4>/dev/null;
    fi

    # grep the inventory file for the hosts
    local host_list=("$(grep -vie "^#" -vie "^$" -vie "^\[" "${arg_inventory_file}" | grep -o "ip=.*" | awk -F"=" '{print $2}')");

    # start the tmux server
    tmux start-server;
    # create a new session and name it, don't attach
    tmux new-session -s "${TMUX_SESSION_NAME}" -d
    local command;
    for i in ${host_list[@]}; do
        #printf "Host: %s\\n" "$(getHostname "${i}")";
        # start a new window
        command="top";
        #tmux split-window -t "${TMUX_SESSION_NAME}" "${command}";
        tmux new-window -t "${TMUX_SESSION_NAME}" -n "${i}";
        #tmux set -g pane-border-status top;
        #tmux set -g pane-border-format "#{pane_index} #{pane_title}"
        #tmux set -g aggressive-resize;
    done
    # The first pane in the window is just a bash shell
    # easier to kill it than seperately parse for the 
    # first host to add to the new-session command
    #tmux kill-pane -t 0
    tmux kill-window -t 0;
    # Tile all of our panes
    tmux select-layout -t "${TMUX_SESSION_NAME}" tiled;
    # finally, attach to the server
    tmux a -t "${TMUX_SESSION_NAME}"
    exit
}

# only run the main script if it's executed directly.
# main() won't run if this script is sourced 
if [[ "${BASH_SOURCE[0]}" = "$0" ]]; then
    main "$@"
fi